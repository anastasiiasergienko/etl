#ETL (Extract-Transform-Load)  
This is a simple ETL application: it extracts flights from source database, transforms them and then adds into a destination database.  
 
Write your database url, user and password to `application.properties`.
  
To build the application, `git pull` the repository and run `mvn package` in the project directory.  
To run the application, build it and then execute `java -jar target/etl-test-0.0.1-SNAPSHOT.jar` in the project directory.  


  