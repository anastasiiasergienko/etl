package com.passengersfriend.etltest.repository;

import com.passengersfriend.etltest.model.ProcessData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessDataRepository extends JpaRepository<ProcessData, Long> {
}
