package com.passengersfriend.etltest.repository;

import com.passengersfriend.etltest.model.SourceDataFlight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SourceDataRepository extends JpaRepository<SourceDataFlight, Long> {

    SourceDataFlight findFirstByOrderByIdAsc();

    SourceDataFlight findFirstByIdGreaterThanOrderByIdAsc(Long id);

    long countAllByIdGreaterThan(Long id);

}
