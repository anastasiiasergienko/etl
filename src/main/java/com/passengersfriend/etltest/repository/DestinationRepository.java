package com.passengersfriend.etltest.repository;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DestinationRepository extends JpaRepository<DestinationDataFlight, Long> {

    List<DestinationDataFlight> findAllByDepartureAirportCodeIATAAndArrivalAirportCodeIATAAndFlightAirlineCodeIATAAndFlightNumber
            (String departureAirportCodeIATA, String arrivalAirportCodeIATA, String flightAirlineCodeIATA, String flightNumber);
}
