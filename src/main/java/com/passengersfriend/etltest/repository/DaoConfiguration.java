package com.passengersfriend.etltest.repository;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
@EntityScan(basePackages = "com.passengersfriend.etltest.model")
public class DaoConfiguration {
}
