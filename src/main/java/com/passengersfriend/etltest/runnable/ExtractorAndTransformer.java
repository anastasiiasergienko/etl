package com.passengersfriend.etltest.runnable;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.model.SourceDataFlight;
import com.passengersfriend.etltest.service.Extractor;
import com.passengersfriend.etltest.service.Transformer;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.passengersfriend.etltest.service.DefaultValues.DEFAULT_BLOCKING_QUEUE_SIZE;

@Component
public class ExtractorAndTransformer implements Runnable {
    private BlockingQueue<DestinationDataFlight> blockingQueue;
    private AtomicBoolean isFinished;

    private Extractor extractor;
    private Transformer transformer;

    public ExtractorAndTransformer(Extractor extractor, BlockingQueue<DestinationDataFlight> blockingQueue, AtomicBoolean isFinished, Transformer transformer) {
        this.extractor = extractor;
        this.blockingQueue = blockingQueue;
        this.isFinished = isFinished;
        this.transformer = transformer;
    }

    @Override
    public void run() {
        SourceDataFlight sourceDataFlight = extractor.getFirstUnprocessedFlight();
        do {
            if (blockingQueue.size() < DEFAULT_BLOCKING_QUEUE_SIZE && sourceDataFlight != null) {
                blockingQueue.add(transformer.transform(sourceDataFlight));
                sourceDataFlight = extractor.getFirstUnprocessedFlight();
            }
        } while (sourceDataFlight != null);

        isFinished.set(true);
    }
}
