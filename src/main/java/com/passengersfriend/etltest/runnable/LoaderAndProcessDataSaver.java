package com.passengersfriend.etltest.runnable;

import com.passengersfriend.etltest.exception.DataLoadException;
import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.service.Loader;
import com.passengersfriend.etltest.service.ProcessDataSaver;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.concurrent.TimeUnit.SECONDS;

@Component
public class LoaderAndProcessDataSaver implements Runnable {
    private BlockingQueue<DestinationDataFlight> blockingQueue;
    private AtomicBoolean isFinished;
    private AtomicLong counter;

    private Loader loader;
    private ProcessDataSaver processDataSaver;

    public LoaderAndProcessDataSaver(
            BlockingQueue<DestinationDataFlight> blockingQueue,
            AtomicBoolean isFinished,
            Loader loader,
            ProcessDataSaver processDataSaver,
            AtomicLong counter
    ) {
        this.blockingQueue = blockingQueue;
        this.isFinished = isFinished;
        this.loader = loader;
        this.processDataSaver = processDataSaver;
        this.counter = counter;
    }

    @Override
    public void run() {
        while (!isFinished.get()) {
            DestinationDataFlight destinationDataFlight;
            try {
                destinationDataFlight = blockingQueue.poll(10L, SECONDS);
            } catch (InterruptedException e) {
                throw new DataLoadException();
            }

            if (destinationDataFlight != null) {
                processNextFlight(destinationDataFlight);
            }
        }
    }

    @Transactional
    public void processNextFlight(DestinationDataFlight destinationDataFlight) {
        loader.saveToDestinationTable(destinationDataFlight);
        processDataSaver.saveLastProcessedId(destinationDataFlight.getId());
        counter.incrementAndGet();
    }
}
