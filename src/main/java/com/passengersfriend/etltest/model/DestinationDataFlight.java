package com.passengersfriend.etltest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "aenaflight_source")
public class DestinationDataFlight {
    //primary key, unique identifier

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "bigint")
    private Long id;
    @Column(name = "adep", nullable = false, columnDefinition = "varchar(8)")
    private String departureAirportCodeIATA;
    @Column(name = "ades", nullable = false, columnDefinition = "varchar(8)")
    private String arrivalAirportCodeIATA;
    @Column(name = "flight_code", nullable = false, columnDefinition = "varchar(8)")
    private String flightAirlineCodeIATA;
    @Column(name = "flight_number", nullable = false, columnDefinition = "varchar(8)")
    private String flightNumber;
    @Column(name = "carrier_code", columnDefinition = "varchar(8)")
    private String carrierCodeIATA;
    @Column(name = "carrier_number", columnDefinition = "varchar(8)")
    private String carrierNumber;
    @Column(name = "status_info", nullable = false, columnDefinition = "varchar(256)")
    private String flightStatusInfo;
    @Column(name = "schd_dep_lt", nullable = false, columnDefinition = "timestamp without time zone")
    private LocalDateTime scheduledDepartureDateTime;
    @Column(name = "schd_arr_lt", nullable = false, columnDefinition = "timestamp without time zone")
    private LocalDateTime scheduledArrivalDateTime;
    @Column(name = "est_dep_lt", columnDefinition = "timestamp without time zone")
    private LocalDateTime estimatedDepartureDateTime;
    @Column(name = "est_arr_lt", columnDefinition = "timestamp without time zone")
    private LocalDateTime estimatedArrivalDateTime;
    @Column(name = "act_dep_lt", columnDefinition = "timestamp without time zone")
    private LocalDateTime actualDepartureDateTime;
    @Column(name = "act_arr_lt", columnDefinition = "timestamp without time zone")
    private LocalDateTime actualArrivalDateTime;
    @Column(name = "flt_leg_seq_no", nullable = false, columnDefinition = "int")
    private Integer flightLegSequenceId;
    @Column(name = "aircraft_name_scheduled", columnDefinition = "text")
    private String aircraftNameScheduled;
    @Column(name = "baggage_info", columnDefinition = "varchar(128)")
    private String baggageInfo;
    @Column(name = "counter", columnDefinition = "varchar(128)")
    private String registrationCounter;
    @Column(name = "gate_info", columnDefinition = "varchar(128)")
    private String gateInfo;
    @Column(name = "lounge_info", columnDefinition = "varchar(128)")
    private String loungeInfo;
    @Column(name = "terminal_info", columnDefinition = "varchar(128)")
    private String terminalInfo;
    @Column(name = "arr_terminal_info", columnDefinition = "varchar(128)")
    private String arrivalTerminalInfo;
    @Column(name = "source_data", columnDefinition = "text")
    private String sourceData;
    @Column(name = "created_at", nullable = false, columnDefinition = "timestamp without time zone")
    private Timestamp createdAt;

    public void setId(Long id) {
        this.id = id;
    }

    public void setDepartureAirportCodeIATA(String departureAirportCodeIATA) {
        this.departureAirportCodeIATA = departureAirportCodeIATA;
    }

    public void setArrivalAirportCodeIATA(String arrivalAirportCodeIATA) {
        this.arrivalAirportCodeIATA = arrivalAirportCodeIATA;
    }

    public void setFlightAirlineCodeIATA(String flightAirlineCodeIATA) {
        this.flightAirlineCodeIATA = flightAirlineCodeIATA;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setCarrierCodeIATA(String carrierCodeIATA) {
        this.carrierCodeIATA = carrierCodeIATA;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    public void setFlightStatusInfo(String flightStatusInfo) {
        this.flightStatusInfo = flightStatusInfo;
    }

    public void setScheduledDepartureDateTime(LocalDateTime scheduledDepartureDateTime) {
        this.scheduledDepartureDateTime = scheduledDepartureDateTime;
    }

    public void setScheduledArrivalDateTime(LocalDateTime scheduledArrivalDateTime) {
        this.scheduledArrivalDateTime = scheduledArrivalDateTime;
    }

    public void setEstimatedDepartureDateTime(LocalDateTime estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public void setEstimatedArrivalDateTime(LocalDateTime estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public void setActualDepartureDateTime(LocalDateTime actualDepartureDateTime) {
        this.actualDepartureDateTime = actualDepartureDateTime;
    }

    public void setActualArrivalDateTime(LocalDateTime actualArrivalDateTime) {
        this.actualArrivalDateTime = actualArrivalDateTime;
    }

    public void setFlightLegSequenceId(Integer flightLegSequenceId) {
        this.flightLegSequenceId = flightLegSequenceId;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public void setRegistrationCounter(String registrationCounter) {
        this.registrationCounter = registrationCounter;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    public void setArrivalTerminalInfo(String arrivalTerminalInfo) {
        this.arrivalTerminalInfo = arrivalTerminalInfo;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Long getId() {
        return id;
    }


    public String getDepartureAirportCodeIATA() {
        return departureAirportCodeIATA;
    }

    public String getArrivalAirportCodeIATA() {
        return arrivalAirportCodeIATA;
    }

    public String getFlightAirlineCodeIATA() {
        return flightAirlineCodeIATA;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getCarrierCodeIATA() {
        return carrierCodeIATA;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public String getFlightStatusInfo() {
        return flightStatusInfo;
    }

    public LocalDateTime getScheduledDepartureDateTime() {
        return scheduledDepartureDateTime;
    }

    public LocalDateTime getScheduledArrivalDateTime() {
        return scheduledArrivalDateTime;
    }

    public LocalDateTime getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public LocalDateTime getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public LocalDateTime getActualDepartureDateTime() {
        return actualDepartureDateTime;
    }

    public LocalDateTime getActualArrivalDateTime() {
        return actualArrivalDateTime;
    }

    public Integer getFlightLegSequenceId() {
        return flightLegSequenceId;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public String getRegistrationCounter() {
        return registrationCounter;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public String getArrivalTerminalInfo() {
        return arrivalTerminalInfo;
    }

    public String getSourceData() {
        return sourceData;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "DestinationDataFlight{" +
                "id=" + id +
                ", departureAirportCodeIATA='" + departureAirportCodeIATA + '\'' +
                ", arrivalAirportCodeIATA='" + arrivalAirportCodeIATA + '\'' +
                ", flightAirlineCodeIATA='" + flightAirlineCodeIATA + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", carrierCodeIATA='" + carrierCodeIATA + '\'' +
                ", carrierNumber=" + carrierNumber +
                ", flightStatusInfo='" + flightStatusInfo + '\'' +
                ", scheduledDepartureDateTime=" + scheduledDepartureDateTime +
                ", scheduledArrivalDateTime=" + scheduledArrivalDateTime +
                ", estimatedDepartureDateTime=" + estimatedDepartureDateTime +
                ", estimatedArrivalDateTime=" + estimatedArrivalDateTime +
                ", actualDepartureDateTime=" + actualDepartureDateTime +
                ", actualArrivalDateTime=" + actualArrivalDateTime +
                ", flightLegSequenceId=" + flightLegSequenceId +
                ", aircraftNameScheduled='" + aircraftNameScheduled + '\'' +
                ", baggageInfo=" + baggageInfo +
                ", registrationCounter='" + registrationCounter + '\'' +
                ", gateInfo='" + gateInfo + '\'' +
                ", loungeInfo='" + loungeInfo + '\'' +
                ", terminalInfo='" + terminalInfo + '\'' +
                ", arrivalTerminalInfo='" + arrivalTerminalInfo + '\'' +
                ", sourceData='" + sourceData + '\'' +
                ", createdAt=" + createdAt +
                '}';
    }
}
