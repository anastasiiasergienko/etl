package com.passengersfriend.etltest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aenaflight_2017_01")
public class SourceDataFlight {

    //primary key, unique identifier
    @Id
    @Column(name = "id", nullable = false, columnDefinition = "bigserial")
    private Long id;
    @Column(name = "act_arr_date_time_lt", columnDefinition = "varchar(64)")
    private String actualArrivalDateTime;
    @Column(name = "aircraft_name_scheduled", columnDefinition = "text")
    private String aircraftNameScheduled;
    @Column(name = "arr_apt_name_es", columnDefinition = "varchar(128)")
    private String arrivalAirportNameSpanish;
    @Column(name = "arr_apt_code_iata", columnDefinition = "varchar(8)")
    private String arrivalAirportCodeIATA;
    @Column(name = "baggage_info", columnDefinition = "varchar(128)")
    private String baggageInfo;
    @Column(name = "carrier_airline_name_en", columnDefinition = "varchar(128)")
    private String arrivalAirportNameEnglish;
    @Column(name = "carrier_icao_code", columnDefinition = "varchar(8)")
    private String carrierCodeIATA;
    @Column(name = "carrier_number", columnDefinition = "varchar(8)")
    private String carrierNumber;
    @Column(name = "counter", columnDefinition = "varchar(64)")
    private String registrationCounter;
    @Column(name = "dep_apt_name_es", columnDefinition = "varchar(128)")
    private String departureAirportNameSpanish;
    @Column(name = "dep_apt_code_iata", columnDefinition = "varchar(8)")
    private String departureAirportCodeIATA;
    @Column(name = "est_arr_date_time_lt", columnDefinition = "varchar(64)")
    private String estimatedArrivalDateTime;
    @Column(name = "est_dep_date_time_lt", columnDefinition = "varchar(64)")
    private String estimatedDepartureDateTime;
    @Column(name = "flight_airline_name_en", columnDefinition = "varchar(128)")
    private String flightAirlineNameEnglish;
    @Column(name = "flight_airline_name", columnDefinition = "varchar(128)")
    private String flightAirlineName;
    @Column(name = "flight_icao_code", columnDefinition = "varchar(8)")
    private String flightAirlineCodeIATA;
    @Column(name = "flight_number", columnDefinition = "varchar(8)")
    private String flightNumber;
    @Column(name = "flt_leg_seq_no", columnDefinition = "varchar(8)")
    private String flightLegSequenceId;
    @Column(name = "gate_info", columnDefinition = "varchar(128)")
    private String gateInfo;
    @Column(name = "lounge_info", columnDefinition = "varchar(128)")
    private String loungeInfo;
    @Column(name = "schd_arr_only_date_lt", columnDefinition = "varchar(32)")
    private String scheduledArrivalDate;
    @Column(name = "schd_arr_only_time_lt", columnDefinition = "varchar(32)")
    private String scheduledArrivalTime;
    @Column(name = "source_data", columnDefinition = "text")
    private String sourceData;
    @Column(name = "status_info", columnDefinition = "varchar(128)")
    private String flightStatusInfo;
    @Column(name = "terminal_info", columnDefinition = "varchar(128)")
    private String terminalInfo;
    @Column(name = "arr_terminal_info", columnDefinition = "varchar(128)")
    private String arrivalTerminalInfo;
    @Column(name = "created_at", columnDefinition = "bigint")
    //unix timestamp when record was created
    private Long createdAt;
    @Column(name = "act_dep_date_time_lt", columnDefinition = "varchar(64)")
    private String actualDepartureDateTime;
    @Column(name = "schd_dep_only_date_lt", columnDefinition = "varchar(32)")
    private String scheduledDepartureDate;
    @Column(name = "schd_dep_only_time_lt", columnDefinition = "varchar(32)")
    private String scheduledDepartureTime;

    public Long getId() {
        return id;
    }

    public String getActualArrivalDateTime() {
        return actualArrivalDateTime;
    }

    public String getAircraftNameScheduled() {
        return aircraftNameScheduled;
    }

    public String getArrivalAirportCodeIATA() {
        return arrivalAirportCodeIATA;
    }

    public String getBaggageInfo() {
        return baggageInfo;
    }

    public String getCarrierCodeIATA() {
        return carrierCodeIATA;
    }

    public String getCarrierNumber() {
        return carrierNumber;
    }

    public String getRegistrationCounter() {
        return registrationCounter;
    }

    public String getDepartureAirportCodeIATA() {
        return departureAirportCodeIATA;
    }

    public String getEstimatedArrivalDateTime() {
        return estimatedArrivalDateTime;
    }

    public String getEstimatedDepartureDateTime() {
        return estimatedDepartureDateTime;
    }

    public String getFlightAirlineCodeIATA() {
        return flightAirlineCodeIATA;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public String getFlightLegSequenceId() {
        return flightLegSequenceId;
    }

    public String getGateInfo() {
        return gateInfo;
    }

    public String getLoungeInfo() {
        return loungeInfo;
    }

    public String getScheduledArrivalDate() {
        return scheduledArrivalDate;
    }

    public String getScheduledArrivalTime() {
        return scheduledArrivalTime;
    }

    public String getSourceData() {
        return sourceData;
    }

    public String getFlightStatusInfo() {
        return flightStatusInfo;
    }

    public String getTerminalInfo() {
        return terminalInfo;
    }

    public String getArrivalTerminalInfo() {
        return arrivalTerminalInfo;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public String getActualDepartureDateTime() {
        return actualDepartureDateTime;
    }

    public String getScheduledDepartureDate() {
        return scheduledDepartureDate;
    }

    public String getScheduledDepartureTime() {
        return scheduledDepartureTime;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setActualArrivalDateTime(String actualArrivalDateTime) {
        this.actualArrivalDateTime = actualArrivalDateTime;
    }

    public void setAircraftNameScheduled(String aircraftNameScheduled) {
        this.aircraftNameScheduled = aircraftNameScheduled;
    }

    public void setArrivalAirportCodeIATA(String arrivalAirportCodeIATA) {
        this.arrivalAirportCodeIATA = arrivalAirportCodeIATA;
    }

    public void setBaggageInfo(String baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public void setCarrierCodeIATA(String carrierCodeIATA) {
        this.carrierCodeIATA = carrierCodeIATA;
    }

    public void setCarrierNumber(String carrierNumber) {
        this.carrierNumber = carrierNumber;
    }

    public void setRegistrationCounter(String registrationCounter) {
        this.registrationCounter = registrationCounter;
    }

    public void setDepartureAirportCodeIATA(String departureAirportCodeIATA) {
        this.departureAirportCodeIATA = departureAirportCodeIATA;
    }

    public void setEstimatedArrivalDateTime(String estimatedArrivalDateTime) {
        this.estimatedArrivalDateTime = estimatedArrivalDateTime;
    }

    public void setEstimatedDepartureDateTime(String estimatedDepartureDateTime) {
        this.estimatedDepartureDateTime = estimatedDepartureDateTime;
    }

    public void setFlightAirlineName(String flightAirlineName) {
        this.flightAirlineName = flightAirlineName;
    }

    public void setFlightAirlineCodeIATA(String flightAirlineCodeIATA) {
        this.flightAirlineCodeIATA = flightAirlineCodeIATA;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setFlightLegSequenceId(String flightLegSequenceId) {
        this.flightLegSequenceId = flightLegSequenceId;
    }

    public void setGateInfo(String gateInfo) {
        this.gateInfo = gateInfo;
    }

    public void setLoungeInfo(String loungeInfo) {
        this.loungeInfo = loungeInfo;
    }

    public void setScheduledArrivalDate(String scheduledArrivalDate) {
        this.scheduledArrivalDate = scheduledArrivalDate;
    }

    public void setScheduledArrivalTime(String scheduledArrivalTime) {
        this.scheduledArrivalTime = scheduledArrivalTime;
    }

    public void setSourceData(String sourceData) {
        this.sourceData = sourceData;
    }

    public void setFlightStatusInfo(String flightStatusInfo) {
        this.flightStatusInfo = flightStatusInfo;
    }

    public void setTerminalInfo(String terminalInfo) {
        this.terminalInfo = terminalInfo;
    }

    public void setArrivalTerminalInfo(String arrivalTerminalInfo) {
        this.arrivalTerminalInfo = arrivalTerminalInfo;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setActualDepartureDateTime(String actualDepartureDateTime) {
        this.actualDepartureDateTime = actualDepartureDateTime;
    }

    public void setScheduledDepartureDate(String scheduledDepartureDate) {
        this.scheduledDepartureDate = scheduledDepartureDate;
    }

    public void setScheduledDepartureTime(String scheduledDepartureTime) {
        this.scheduledDepartureTime = scheduledDepartureTime;
    }

    @Override
    public String toString() {
        return "SourceDataFlight{" +
                "id=" + id +
                ", departureAirportCodeIATA='" + departureAirportCodeIATA + '\'' +
                ", arrivalAirportCodeIATA='" + arrivalAirportCodeIATA + '\'' +
                ", flightAirlineCodeIATA='" + flightAirlineCodeIATA + '\'' +
                ", flightNumber='" + flightNumber + '\'' +
                ", carrierCodeIATA='" + carrierCodeIATA + '\'' +
                ", carrierNumber=" + carrierNumber +
                ", flightStatusInfo='" + flightStatusInfo + '\'' +
                ", scheduledDepartureDate='" + scheduledDepartureDate + '\'' +
                ", scheduledDepartureTime='" + scheduledDepartureTime + '\'' +
                ", scheduledArrivalDate='" + scheduledArrivalDate + '\'' +
                ", scheduledArrivalTime='" + scheduledArrivalTime + '\'' +
                ", estimatedDepartureDateTime='" + estimatedDepartureDateTime + '\'' +
                ", estimatedArrivalDateTime='" + estimatedArrivalDateTime + '\'' +
                ", actualDepartureDateTime='" + actualDepartureDateTime + '\'' +
                ", actualArrivalDateTime='" + actualArrivalDateTime + '\'' +
                ", flightLegSequenceId='" + flightLegSequenceId + '\'' +
                ", aircraftNameScheduled='" + aircraftNameScheduled + '\'' +
                ", baggageInfo='" + baggageInfo + '\'' +
                ", registrationCounter='" + registrationCounter + '\'' +
                ", gateInfo='" + gateInfo + '\'' +
                ", loungeInfo='" + loungeInfo + '\'' +
                ", terminalInfo='" + terminalInfo + '\'' +
                ", arrivalTerminalInfo='" + arrivalTerminalInfo + '\'' +
                ", sourceData='" + sourceData + '\'' +
                ", createdAt=" + createdAt +
                ", arrivalAirportNameSpanish='" + arrivalAirportNameSpanish + '\'' +
                ", arrivalAirportNameEnglish='" + arrivalAirportNameEnglish + '\'' +
                ", departureAirportNameSpanish='" + departureAirportNameSpanish + '\'' +
                ", flightAirlineNameEnglish='" + flightAirlineNameEnglish + '\'' +
                ", flightAirlineName='" + flightAirlineName + '\'' +
                '}';
    }
}
