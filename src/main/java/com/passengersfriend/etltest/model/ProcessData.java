package com.passengersfriend.etltest.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "process_data")
public class ProcessData {

    @Id
    @Column(name = "id", nullable = false, columnDefinition = "serial")
    private Long id;
    @Column(name = "last_processed_id", columnDefinition = "bigint")
    private Long lastProcessedId;

    public ProcessData(Long id, Long lastProcessedId) {
        this.id = id;
        this.lastProcessedId = lastProcessedId;
    }

    public ProcessData() {

    }

    public Long getLastProcessedId() {
        return lastProcessedId;
    }
}
