package com.passengersfriend.etltest.exception;

public class DataLoadException extends RuntimeException {

    public DataLoadException() {
    }

    public DataLoadException(String message) {
        super(message);
    }
}
