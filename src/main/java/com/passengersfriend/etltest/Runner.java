package com.passengersfriend.etltest;

import com.passengersfriend.etltest.runnable.ExtractorAndTransformer;
import com.passengersfriend.etltest.runnable.LoaderAndProcessDataSaver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class Runner implements CommandLineRunner {
    private ExtractorAndTransformer extractorAndTransformer;
    private LoaderAndProcessDataSaver loaderAndProcessDataSaver;

    public Runner(ExtractorAndTransformer extractorAndTransformer, LoaderAndProcessDataSaver loaderAndProcessDataSaver) {
        this.extractorAndTransformer = extractorAndTransformer;
        this.loaderAndProcessDataSaver = loaderAndProcessDataSaver;
    }

    @Override
    public void run(String... args) {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(extractorAndTransformer);
        executorService.submit(loaderAndProcessDataSaver);
    }
}
