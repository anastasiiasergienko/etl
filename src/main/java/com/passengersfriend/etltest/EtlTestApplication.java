package com.passengersfriend.etltest;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static com.passengersfriend.etltest.service.DefaultValues.DEFAULT_BLOCKING_QUEUE_SIZE;

@EnableTransactionManagement
@EnableScheduling
@SpringBootApplication
public class EtlTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtlTestApplication.class, args);
    }

    @Bean
    public AtomicLong processedCounter() {
        return new AtomicLong();
    }

    @Bean
    public AtomicBoolean isFinished() {
        return new AtomicBoolean();
    }

    @Bean
    public BlockingQueue<DestinationDataFlight> flights() {
        return new LinkedBlockingDeque<>(DEFAULT_BLOCKING_QUEUE_SIZE);
    }

    //todo CommandLineRunner as @Bean: public CommandLineRunner runner(Runner runner) { return args -> { some logic here } }
}
