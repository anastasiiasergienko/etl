package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.model.SourceDataFlight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@Service
public class TransformerImpl implements Transformer {
    private Logger logger = LogManager.getLogger(TransformerImpl.class);

    @Override
    public DestinationDataFlight transform(SourceDataFlight sourceDataFlight) {
        DestinationDataFlight destinationDataFlight = new DestinationDataFlight();

        destinationDataFlight.setId(sourceDataFlight.getId());
        destinationDataFlight.setDepartureAirportCodeIATA(sourceDataFlight.getDepartureAirportCodeIATA());
        destinationDataFlight.setArrivalAirportCodeIATA(sourceDataFlight.getArrivalAirportCodeIATA());
        destinationDataFlight.setFlightAirlineCodeIATA(sourceDataFlight.getFlightAirlineCodeIATA());
        destinationDataFlight.setFlightNumber(sourceDataFlight.getFlightNumber());
        destinationDataFlight.setCarrierCodeIATA(sourceDataFlight.getCarrierCodeIATA());
        destinationDataFlight.setCarrierNumber(sourceDataFlight.getCarrierNumber());
        destinationDataFlight.setFlightNumber(sourceDataFlight.getFlightNumber());
        destinationDataFlight.setFlightStatusInfo(sourceDataFlight.getFlightStatusInfo());

        destinationDataFlight.setScheduledDepartureDateTime(getLocalDateTimeNotNull(getLocalDateTimeFromString(mergeDateAndTime
                (sourceDataFlight.getScheduledDepartureDate(), sourceDataFlight.getScheduledDepartureTime()))));
        destinationDataFlight.setScheduledArrivalDateTime(getLocalDateTimeNotNull(getLocalDateTimeFromString(mergeDateAndTime
                (sourceDataFlight.getScheduledArrivalDate(), sourceDataFlight.getScheduledArrivalTime()))));

        destinationDataFlight.setEstimatedDepartureDateTime(getLocalDateTimeFromString
                (sourceDataFlight.getEstimatedDepartureDateTime()));
        destinationDataFlight.setEstimatedArrivalDateTime(getLocalDateTimeFromString
                (sourceDataFlight.getEstimatedArrivalDateTime()));
        destinationDataFlight.setActualDepartureDateTime(getLocalDateTimeFromString(sourceDataFlight.getActualDepartureDateTime()));
        destinationDataFlight.setActualArrivalDateTime(getLocalDateTimeFromString(sourceDataFlight.getActualArrivalDateTime()));

        destinationDataFlight.setFlightLegSequenceId(Integer.valueOf(sourceDataFlight.getFlightLegSequenceId()));
        destinationDataFlight.setAircraftNameScheduled(sourceDataFlight.getAircraftNameScheduled());

        destinationDataFlight.setBaggageInfo(sourceDataFlight.getBaggageInfo());
        destinationDataFlight.setRegistrationCounter(sourceDataFlight.getRegistrationCounter());
        destinationDataFlight.setGateInfo(sourceDataFlight.getGateInfo());
        destinationDataFlight.setLoungeInfo(sourceDataFlight.getLoungeInfo());
        destinationDataFlight.setTerminalInfo(sourceDataFlight.getTerminalInfo());
        destinationDataFlight.setArrivalTerminalInfo(sourceDataFlight.getArrivalTerminalInfo());
        destinationDataFlight.setSourceData(sourceDataFlight.getSourceData());

        destinationDataFlight.setCreatedAt(toTimestamp(sourceDataFlight.getCreatedAt()));

        logger.trace("Transformation is done. " + destinationDataFlight);
        return destinationDataFlight;
    }

    private LocalDateTime getLocalDateTimeNotNull(LocalDateTime date) {
        if (date == null) {
            return LocalDateTime.of(0, 1, 1, 0, 0);
        } else {
            return date;
        }
    }

    private LocalDateTime getLocalDateTimeFromString(String date) {
        if (date.length() > 1) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm");
            return LocalDateTime.parse(date, formatter);
        } else {
            return null;
        }
    }

    private String mergeDateAndTime(String scheduledDepartureDate, String scheduledDepartureTime) {
        return scheduledDepartureDate + " " + scheduledDepartureTime;
    }

    private Timestamp toTimestamp(long epochSecond) {
        return Timestamp.valueOf(LocalDateTime.ofEpochSecond(epochSecond, 0, ZoneOffset.UTC));
    }
}
