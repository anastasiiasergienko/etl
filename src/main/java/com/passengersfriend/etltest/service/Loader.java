package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.DestinationDataFlight;

public interface Loader {
    void saveToDestinationTable(DestinationDataFlight destinationDataFlight);
}
