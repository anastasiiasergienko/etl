package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.ProcessData;
import com.passengersfriend.etltest.model.SourceDataFlight;
import com.passengersfriend.etltest.repository.ProcessDataRepository;
import com.passengersfriend.etltest.repository.SourceDataRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Optional;

@Service
public class ExtractorImpl implements Extractor {
    private Logger logger = LogManager.getLogger(ExtractorImpl.class);

    private SourceDataRepository sourceDataRepository;
    private ProcessDataRepository processDataRepository;
    private Long lastProcessedId;

    public ExtractorImpl(SourceDataRepository sourceDataRepository, ProcessDataRepository processDataRepository) {
        this.sourceDataRepository = sourceDataRepository;
        this.processDataRepository = processDataRepository;
    }

    @PostConstruct
    public void init() {
        lastProcessedId = checkForLastProcessedId();
    }

    @Override
    public SourceDataFlight getFirstUnprocessedFlight() {
        if (lastProcessedId != null) {
            logger.trace("Last processed id has been found: " + lastProcessedId);

            SourceDataFlight sourceDataFlight = sourceDataRepository.findFirstByIdGreaterThanOrderByIdAsc(lastProcessedId);
            logger.trace(sourceDataFlight);
            lastProcessedId = sourceDataFlight.getId();
            return sourceDataFlight;
        } else {
            logger.trace("There is no last processed id available.");
            SourceDataFlight sourceDataFlight = sourceDataRepository.findFirstByOrderByIdAsc();
            if (sourceDataFlight != null) {
                lastProcessedId = sourceDataFlight.getId();
            }
            return sourceDataFlight;
        }
    }

    private Long checkForLastProcessedId() {
        Optional<ProcessData> processData = processDataRepository.findById(1L);
        if (processData.isPresent()) {
            return processData.get().getLastProcessedId();
        } else {
            return null;
        }
    }
}
