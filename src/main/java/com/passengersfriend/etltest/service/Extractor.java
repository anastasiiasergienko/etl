package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.SourceDataFlight;

public interface Extractor {
    SourceDataFlight getFirstUnprocessedFlight();
}
