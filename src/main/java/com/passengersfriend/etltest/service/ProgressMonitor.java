//package com.passengersfriend.etltest.service;
//
//import com.passengersfriend.etltest.model.ProcessData;
//import com.passengersfriend.etltest.repository.ProcessDataRepository;
//import com.passengersfriend.etltest.repository.SourceDataRepository;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.PostConstruct;
//import java.util.Optional;
//import java.util.concurrent.atomic.AtomicLong;
//
//@Service
//public class ProgressMonitor {
//    private Logger log = LoggerFactory.getLogger(ProgressMonitor.class);
//
//    private AtomicLong progressCounter;
//    private SourceDataRepository sourceDataRepository;
//    private ProcessDataRepository processDataRepository;
//    private long totalFlightsToProcess;
//
//    public ProgressMonitor(AtomicLong progressCounter, SourceDataRepository sourceDataRepository, ProcessDataRepository processDataRepository) {
//        this.progressCounter = progressCounter;
//        this.sourceDataRepository = sourceDataRepository;
//        this.processDataRepository = processDataRepository;
//    }
//
//    @PostConstruct
//    public void init() {
//        Optional<ProcessData> processData = processDataRepository.findById(1L);
//
//        if (processData.isPresent()) {
//            totalFlightsToProcess = sourceDataRepository.countAllByIdGreaterThan(processData.get().getLastProcessedId());
//        } else {
//            totalFlightsToProcess = sourceDataRepository.count();
//        }
//    }
//
//    @Scheduled(fixedRate = 10000)
//    public void printOutProgress() {
//        long totalPrc = totalFlightsToProcess / 100;
//        log.info("Processed {} out of {} flights. {}%", progressCounter.get(), totalFlightsToProcess, progressCounter.get() / totalPrc);
//    }
//}
