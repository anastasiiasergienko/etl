package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.ProcessData;
import com.passengersfriend.etltest.repository.ProcessDataRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class ProcessDataSaver {
    private Logger logger = LogManager.getLogger(ProcessDataSaver.class);

    private ProcessDataRepository processDataRepository;

    public ProcessDataSaver(ProcessDataRepository processDataRepository) {
        this.processDataRepository = processDataRepository;
    }

    public void saveLastProcessedId(Long lastProcessedId) {
        logger.trace("ProcessDataSaver is ready to save last processed id: " + lastProcessedId);
        processDataRepository.save(new ProcessData(1L, lastProcessedId));
    }
}
