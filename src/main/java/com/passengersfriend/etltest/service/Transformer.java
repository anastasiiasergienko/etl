package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.model.SourceDataFlight;

public interface Transformer {
    DestinationDataFlight transform(SourceDataFlight sourceDataFlight);
}
