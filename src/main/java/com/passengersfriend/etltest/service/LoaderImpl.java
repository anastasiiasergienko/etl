package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.exception.DataLoadException;
import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.repository.DestinationRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.passengersfriend.etltest.service.DefaultValues.DEFAULT_LOCAL_DATE_TIME;

@Service
public class LoaderImpl implements Loader {
    private Logger logger = LogManager.getLogger(LoaderImpl.class);

    private DestinationRepository destinationRepository;

    public LoaderImpl(DestinationRepository destinationRepository) {
        this.destinationRepository = destinationRepository;
    }

    @Override
    public void saveToDestinationTable(DestinationDataFlight destinationDataFlight) {
        Optional<DestinationDataFlight> existingDestinationDataFlight = findOlderFlightVersion(destinationDataFlight);

        if (!existingDestinationDataFlight.isPresent()) {
            destinationRepository.save(destinationDataFlight);
            logger.info("Loader saved new destination data flight.");
        } else {
            logger.trace("Loader found existed data.");
            DestinationDataFlight mergedData = mergeDestinationDataFlight(existingDestinationDataFlight.get(), destinationDataFlight);
            destinationRepository.save(mergedData);
            logger.trace("Loader updated existed destination data flight.");
        }
    }

    private Optional<DestinationDataFlight> findOlderFlightVersion(DestinationDataFlight destinationDataFlight) {
        List<DestinationDataFlight> flights = destinationRepository.
                findAllByDepartureAirportCodeIATAAndArrivalAirportCodeIATAAndFlightAirlineCodeIATAAndFlightNumber(
                        destinationDataFlight.getDepartureAirportCodeIATA(), destinationDataFlight.getArrivalAirportCodeIATA(),
                        destinationDataFlight.getFlightAirlineCodeIATA(), destinationDataFlight.getFlightNumber());

        if (flights.isEmpty()) {
            return Optional.empty();
        } else if (flights.size() == 1) {
            return Optional.of(flights.get(0));
        } else {
            throw new DataLoadException("Destination database has an illegal duplicate.");
        }
    }

    protected DestinationDataFlight mergeDestinationDataFlight(DestinationDataFlight oldData, DestinationDataFlight newData) {
        if (checkIfExists(newData.getCarrierCodeIATA())) {
            oldData.setCarrierCodeIATA(newData.getCarrierCodeIATA());
        }
        if (checkIfExists(newData.getCarrierNumber())) {
            oldData.setCarrierNumber(newData.getCarrierNumber());
        }
        if (checkIfExists(newData.getFlightStatusInfo())) {
            oldData.setFlightStatusInfo(newData.getFlightStatusInfo());
        }
        if (checkIfExists(newData.getAircraftNameScheduled())) {
            oldData.setAircraftNameScheduled(newData.getAircraftNameScheduled());
        }
        if (checkIfExists(newData.getSourceData())) {
            oldData.setSourceData(newData.getSourceData());
        }

        if (checkIfExists(newData.getScheduledDepartureDateTime())) {
            oldData.setScheduledDepartureDateTime(newData.getScheduledDepartureDateTime());
        }
        if (checkIfExists(newData.getScheduledArrivalDateTime())) {
            oldData.setScheduledArrivalDateTime(newData.getScheduledArrivalDateTime());
        }
        if (checkIfExists(newData.getEstimatedDepartureDateTime())) {
            oldData.setEstimatedDepartureDateTime(newData.getEstimatedDepartureDateTime());
        }
        if (checkIfExists(newData.getEstimatedArrivalDateTime())) {
            oldData.setEstimatedArrivalDateTime(newData.getEstimatedArrivalDateTime());
        }
        if (checkIfExists(newData.getActualDepartureDateTime())) {
            oldData.setActualDepartureDateTime(newData.getActualDepartureDateTime());
        }
        if (checkIfExists(newData.getActualArrivalDateTime())) {
            oldData.setActualArrivalDateTime(newData.getActualArrivalDateTime());
        }

        if (newData.getFlightLegSequenceId() != null) {
            oldData.setFlightLegSequenceId(newData.getFlightLegSequenceId());
        }

        if (checkIfExists(newData.getBaggageInfo())) {
            oldData.setBaggageInfo(appendNewData(oldData.getBaggageInfo().trim(), newData.getBaggageInfo().trim()));
        }
        if (checkIfExists(newData.getRegistrationCounter())) {
            oldData.setRegistrationCounter(appendNewData(oldData.getRegistrationCounter().trim(), newData.getRegistrationCounter().trim()));
        }
        if (checkIfExists(newData.getGateInfo())) {
            oldData.setGateInfo(appendNewData(oldData.getGateInfo().trim(), newData.getGateInfo().trim()));
        }
        if (checkIfExists(newData.getLoungeInfo())) {
            oldData.setLoungeInfo(appendNewData(oldData.getLoungeInfo().trim(), newData.getLoungeInfo().trim()));
        }
        if (checkIfExists(newData.getTerminalInfo())) {
            oldData.setTerminalInfo(appendNewData(oldData.getTerminalInfo().trim(), newData.getTerminalInfo().trim()));
        }
        if (checkIfExists(newData.getArrivalTerminalInfo())) {
            oldData.setArrivalTerminalInfo(appendNewData(oldData.getArrivalTerminalInfo().trim(), newData.getArrivalTerminalInfo().trim()));
        }

        oldData.setCreatedAt(newData.getCreatedAt());
        return oldData;
    }

    private boolean checkIfExists(LocalDateTime localDateTime) {
        return localDateTime != null && localDateTime.isAfter(DEFAULT_LOCAL_DATE_TIME);
    }

    private boolean checkIfExists(String newData) {
        return newData != null && newData.trim().length() > 0;
    }

    private String appendNewData(String oldData, String newData) { //check if exists and trim
        List<String> oldDataList = new ArrayList<>(Arrays.asList(oldData.split(",")));

        oldDataList.removeIf(s -> s.equals(newData));

        oldDataList.add(0, newData);
        return oldDataList.stream().collect(Collectors.joining(","));
    }
}
