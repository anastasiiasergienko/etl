package com.passengersfriend.etltest.service;

import java.time.LocalDateTime;

public interface DefaultValues {
    LocalDateTime DEFAULT_LOCAL_DATE_TIME = LocalDateTime.of(1, 1, 1, 0, 0);
    Integer DEFAULT_BLOCKING_QUEUE_SIZE = 50;
}
