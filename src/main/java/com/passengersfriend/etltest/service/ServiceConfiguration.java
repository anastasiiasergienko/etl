package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.repository.DaoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DaoConfiguration.class)
@ComponentScan(basePackages = "com.passengersfriend.etltest.service")
public class ServiceConfiguration {
}
