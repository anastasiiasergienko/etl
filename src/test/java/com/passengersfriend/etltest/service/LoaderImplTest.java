package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.repository.DestinationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoaderImplTest {
    private static final Long OLD_ID = 1L;
    private static final String OLD_DEPARTURE_AIRPORT_CODE_IATA = "FCO";
    private static final String OLD_ARRIVAL_AIRPORT_CODE_IATA = "BCN";
    private static final String OLD_FLIGHT_AIRLINE_CODE_IATA = "BAW";
    private static final String OLD_FLIGHT_NUMBER = "8105";
    private static final String OLD_CARRIER_CODE_IATA = "VLG";
    private static final String OLD_CARRIER_NUMBER = "6107";
    private static final String OLD_FLIGHT_STATUS_INFO = "Departure expected";
    private static final LocalDateTime OLD_ESTIMATE_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 2, 1, 11, 25);
    private static final LocalDateTime OLD_ESTIMATE_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 10, 30);
    private static final LocalDateTime OLD_ACTUAL_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 1, 31, 23, 50);
    private static final LocalDateTime OLD_ACTUAL_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 1, 10);
    private static final LocalDateTime OLD_SCHEDULED_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 1, 31, 22, 35);
    private static final LocalDateTime OLD_SCHEDULED_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 7, 30);
    private static final Integer OLD_FLIGHT_LEG_SEQUENCE_ID = 1;
    private static final String OLD_AIRCRAFT_NAME_SCHEDULED = "AIRBUS A320";
    private static final String OLD_BAGGAGE_INFO = "03";
    private static final String OLD_REGISTRATION_COUNTER = "251 - 252";
    private static final String OLD_GATE_INFO = "D93";
    private static final String OLD_LOUNGE_INFO = "T1_G";
    private static final String OLD_SOURCE_DATA = "http://www.aena.es/csee/Satellite/infovuelos/en/";
    private static final String OLD_TERMINAL_INFO = "T1";
    private static final String OLD_ARRIVAL_TERMINAL_INFO = "T1";
    private static final LocalDateTime OLD_CREATED_AT = LocalDateTime.of(2017, 1, 31, 23, 23, 29);

    private static final String NEW_FLIGHT_STATUS_INFO = "The flight landed";
    private static final LocalDateTime NEW_ACTUAL_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 1, 31, 23, 55);
    private static final LocalDateTime NEW_SCHEDULED_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 8, 30);
    private static final String NEW_BAGGAGE_INFO = "05";
    private static final String NEW_REGISTRATION_COUNTER = "250 - 252";
    private static final String NEW_GATE_INFO = "D96";
    private static final LocalDateTime NEW_CREATED_AT = LocalDateTime.of(2017, 1, 31, 23, 23, 50);

    @Mock
    private DestinationRepository destinationRepository;
    @Mock
    private DestinationDataFlight newDestinationDataFlight;
    private DestinationDataFlight destinationDataFlight;
    private LoaderImpl loader;

    @Before
    public void setUp() {
        loader = new LoaderImpl(destinationRepository);

        destinationDataFlight = new DestinationDataFlight();
        destinationDataFlight.setId(OLD_ID);
        destinationDataFlight.setDepartureAirportCodeIATA(OLD_DEPARTURE_AIRPORT_CODE_IATA);
        destinationDataFlight.setArrivalAirportCodeIATA(OLD_ARRIVAL_AIRPORT_CODE_IATA);
        destinationDataFlight.setFlightAirlineCodeIATA(OLD_FLIGHT_AIRLINE_CODE_IATA);
        destinationDataFlight.setFlightNumber(OLD_FLIGHT_NUMBER);

        destinationDataFlight.setCarrierCodeIATA(OLD_CARRIER_CODE_IATA);
        destinationDataFlight.setCarrierNumber(OLD_CARRIER_NUMBER);
        destinationDataFlight.setFlightStatusInfo(OLD_FLIGHT_STATUS_INFO);
        destinationDataFlight.setEstimatedArrivalDateTime(OLD_ESTIMATE_ARRIVAL_DATE_TIME);
        destinationDataFlight.setEstimatedDepartureDateTime(OLD_ESTIMATE_DEPARTURE_DATE_TIME);
        destinationDataFlight.setActualDepartureDateTime(OLD_ACTUAL_DEPARTURE_DATE_TIME);
        destinationDataFlight.setActualArrivalDateTime(OLD_ACTUAL_ARRIVAL_DATE_TIME);
        destinationDataFlight.setScheduledArrivalDateTime(OLD_SCHEDULED_ARRIVAL_DATE_TIME);
        destinationDataFlight.setScheduledDepartureDateTime(OLD_SCHEDULED_DEPARTURE_DATE_TIME);
        destinationDataFlight.setFlightLegSequenceId(OLD_FLIGHT_LEG_SEQUENCE_ID);
        destinationDataFlight.setAircraftNameScheduled(OLD_AIRCRAFT_NAME_SCHEDULED);
        destinationDataFlight.setBaggageInfo(OLD_BAGGAGE_INFO);
        destinationDataFlight.setRegistrationCounter(OLD_REGISTRATION_COUNTER);
        destinationDataFlight.setGateInfo(OLD_GATE_INFO);
        destinationDataFlight.setLoungeInfo(OLD_LOUNGE_INFO);
        destinationDataFlight.setSourceData(OLD_SOURCE_DATA);
        destinationDataFlight.setTerminalInfo(OLD_TERMINAL_INFO);
        destinationDataFlight.setArrivalTerminalInfo(OLD_ARRIVAL_TERMINAL_INFO);
        destinationDataFlight.setCreatedAt(Timestamp.valueOf(OLD_CREATED_AT));

        when(newDestinationDataFlight.getCarrierCodeIATA()).thenReturn(OLD_CARRIER_CODE_IATA);
        when(newDestinationDataFlight.getCarrierNumber()).thenReturn(OLD_CARRIER_NUMBER);
        when(newDestinationDataFlight.getFlightStatusInfo()).thenReturn(NEW_FLIGHT_STATUS_INFO);
        when(newDestinationDataFlight.getEstimatedArrivalDateTime()).thenReturn(OLD_ESTIMATE_ARRIVAL_DATE_TIME);
        when(newDestinationDataFlight.getEstimatedDepartureDateTime()).thenReturn(OLD_ESTIMATE_DEPARTURE_DATE_TIME);
        when(newDestinationDataFlight.getActualDepartureDateTime()).thenReturn(OLD_ACTUAL_DEPARTURE_DATE_TIME);
        when(newDestinationDataFlight.getActualArrivalDateTime()).thenReturn(NEW_ACTUAL_ARRIVAL_DATE_TIME);
        when(newDestinationDataFlight.getScheduledArrivalDateTime()).thenReturn(OLD_SCHEDULED_ARRIVAL_DATE_TIME);
        when(newDestinationDataFlight.getScheduledDepartureDateTime()).thenReturn(NEW_SCHEDULED_DEPARTURE_DATE_TIME);
        when(newDestinationDataFlight.getFlightLegSequenceId()).thenReturn(OLD_FLIGHT_LEG_SEQUENCE_ID);
        when(newDestinationDataFlight.getAircraftNameScheduled()).thenReturn(OLD_AIRCRAFT_NAME_SCHEDULED);
        when(newDestinationDataFlight.getBaggageInfo()).thenReturn(NEW_BAGGAGE_INFO);
        when(newDestinationDataFlight.getRegistrationCounter()).thenReturn(NEW_REGISTRATION_COUNTER);
        when(newDestinationDataFlight.getGateInfo()).thenReturn(NEW_GATE_INFO);
        when(newDestinationDataFlight.getLoungeInfo()).thenReturn(OLD_LOUNGE_INFO);
        when(newDestinationDataFlight.getSourceData()).thenReturn(OLD_SOURCE_DATA);
        when(newDestinationDataFlight.getTerminalInfo()).thenReturn(OLD_TERMINAL_INFO);
        when(newDestinationDataFlight.getArrivalTerminalInfo()).thenReturn(OLD_ARRIVAL_TERMINAL_INFO);
        when(newDestinationDataFlight.getCreatedAt()).thenReturn(Timestamp.valueOf(NEW_CREATED_AT));
    }

    @Test
    public void saveToDestinationTable() {
        loader.saveToDestinationTable(destinationDataFlight);

        verify(destinationRepository).save(any());
        verify(destinationRepository).
                findAllByDepartureAirportCodeIATAAndArrivalAirportCodeIATAAndFlightAirlineCodeIATAAndFlightNumber
                        (any(), any(), any(), any());
    }

    @Test
    public void mergeDestinationDataFlight() {
        DestinationDataFlight mergedDestinationDataFlight = loader.mergeDestinationDataFlight(destinationDataFlight, newDestinationDataFlight);

        assertEquals(OLD_ID, mergedDestinationDataFlight.getId());
        assertEquals(OLD_DEPARTURE_AIRPORT_CODE_IATA, mergedDestinationDataFlight.getDepartureAirportCodeIATA());
        assertEquals(OLD_ARRIVAL_AIRPORT_CODE_IATA, mergedDestinationDataFlight.getArrivalAirportCodeIATA());
        assertEquals(OLD_FLIGHT_AIRLINE_CODE_IATA, mergedDestinationDataFlight.getFlightAirlineCodeIATA());
        assertEquals(OLD_FLIGHT_NUMBER, mergedDestinationDataFlight.getFlightNumber());
        assertEquals(OLD_CARRIER_CODE_IATA, mergedDestinationDataFlight.getCarrierCodeIATA());
        assertEquals(OLD_CARRIER_NUMBER, mergedDestinationDataFlight.getCarrierNumber());
        assertEquals(NEW_FLIGHT_STATUS_INFO, mergedDestinationDataFlight.getFlightStatusInfo());
        assertEquals(OLD_ESTIMATE_ARRIVAL_DATE_TIME, mergedDestinationDataFlight.getEstimatedArrivalDateTime());
        assertEquals(OLD_ESTIMATE_DEPARTURE_DATE_TIME, mergedDestinationDataFlight.getEstimatedDepartureDateTime());
        assertEquals(OLD_ACTUAL_DEPARTURE_DATE_TIME, mergedDestinationDataFlight.getActualDepartureDateTime());
        assertEquals(NEW_ACTUAL_ARRIVAL_DATE_TIME, mergedDestinationDataFlight.getActualArrivalDateTime());
        assertEquals(OLD_SCHEDULED_ARRIVAL_DATE_TIME, mergedDestinationDataFlight.getScheduledArrivalDateTime());
        assertEquals(NEW_SCHEDULED_DEPARTURE_DATE_TIME, mergedDestinationDataFlight.getScheduledDepartureDateTime());
        assertEquals(OLD_FLIGHT_LEG_SEQUENCE_ID, mergedDestinationDataFlight.getFlightLegSequenceId());
        assertEquals(OLD_AIRCRAFT_NAME_SCHEDULED, mergedDestinationDataFlight.getAircraftNameScheduled());
        assertEquals(NEW_BAGGAGE_INFO + "," + OLD_BAGGAGE_INFO, mergedDestinationDataFlight.getBaggageInfo());
        assertEquals(NEW_REGISTRATION_COUNTER + "," + OLD_REGISTRATION_COUNTER, mergedDestinationDataFlight.getRegistrationCounter());
        assertEquals(NEW_GATE_INFO + "," + OLD_GATE_INFO, mergedDestinationDataFlight.getGateInfo());
        assertEquals(OLD_LOUNGE_INFO, mergedDestinationDataFlight.getLoungeInfo());
        assertEquals(OLD_SOURCE_DATA, mergedDestinationDataFlight.getSourceData());
        assertEquals(OLD_TERMINAL_INFO, mergedDestinationDataFlight.getTerminalInfo());
        assertEquals(OLD_ARRIVAL_TERMINAL_INFO, mergedDestinationDataFlight.getArrivalTerminalInfo());
        assertEquals(Timestamp.valueOf(NEW_CREATED_AT), mergedDestinationDataFlight.getCreatedAt());
    }
}