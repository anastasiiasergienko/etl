package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.repository.ProcessDataRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProcessDataSaverTest {

    @Mock
    private ProcessDataRepository processDataRepository;

    @Test
    public void saveLastProcessedId() {
        ProcessDataSaver processDataSaver = new ProcessDataSaver(processDataRepository);
        processDataSaver.saveLastProcessedId(1L);
        verify(processDataRepository).save(any());
    }
}