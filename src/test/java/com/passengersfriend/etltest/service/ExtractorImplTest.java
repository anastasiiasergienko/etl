package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.ProcessData;
import com.passengersfriend.etltest.model.SourceDataFlight;
import com.passengersfriend.etltest.repository.ProcessDataRepository;
import com.passengersfriend.etltest.repository.SourceDataRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExtractorImplTest {

    @Mock
    private SourceDataRepository sourceDataRepository;
    @Mock
    private ProcessDataRepository processDataRepository;

    @Test
    public void getFirstUnprocessedFlightTestNull() {
        Extractor extractor = new ExtractorImpl(sourceDataRepository, processDataRepository);
        extractor.getFirstUnprocessedFlight();
        verify(sourceDataRepository).findFirstByOrderByIdAsc();
    }

    @Test
    public void getFirstUnprocessedFlightTestNotNull() {
        when(processDataRepository.findById(any())).thenReturn(Optional.of(new ProcessData(1L, 1L)));
        when(sourceDataRepository.findFirstByIdGreaterThanOrderByIdAsc(any())).thenReturn(new SourceDataFlight());
        ExtractorImpl extractor = new ExtractorImpl(sourceDataRepository, processDataRepository);

        extractor.init();
        extractor.getFirstUnprocessedFlight();
        verify(sourceDataRepository).findFirstByIdGreaterThanOrderByIdAsc(any());
    }
}