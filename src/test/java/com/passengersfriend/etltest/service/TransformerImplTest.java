package com.passengersfriend.etltest.service;

import com.passengersfriend.etltest.model.DestinationDataFlight;
import com.passengersfriend.etltest.model.SourceDataFlight;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class TransformerImplTest {
    private static final Long SOURCE_ID = 1L;
    private static final String SOURCE_ACTUAL_ARRIVAL_DATE_TIME = "31/01/17 23:50";
    private static final String SOURCE_AIRCRAFT_NAME_SCHEDULED = "AIRBUS A320";
    private static final String SOURCE_ARRIVAL_AIRPORT_CODE_IATA = "BCN";
    private static final String SOURCE_BAGGAGE_INFO = "03";
    private static final String SOURCE_CARRIER_CODE_IATA = "VLG";
    private static final String SOURCE_CARRIER_NUMBER = "6107";
    private static final String SOURCE_REGISTRATION_COUNTER = "251 - 252";
    private static final String SOURCE_DEPARTURE_AIRPORT_CODE_IATA = "FCO";
    private static final String SOURCE_ESTIMATE_ARRIVAL_DATE_TIME = "01/02/17 11:25";
    private static final String SOURCE_ESTIMATE_DEPARTURE_DATE_TIME = "01/02/17 10:30";
    private static final String SOURCE_FLIGHT_AIRLINE_CODE_IATA = "BAW";
    private static final String SOURCE_FLIGHT_NUMBER = "8105";
    private static final String SOURCE_FLIGHT_LEG_SEQUENCE_ID = "1";
    private static final String SOURCE_GATE_INFO = "D93";
    private static final String SOURCE_LOUNGE_INFO = "T1_G";
    private static final String SOURCE_SCHEDULED_ARRIVAL_DATE = "31/01/17";
    private static final String SOURCE_SCHEDULED_ARRIVAL_TIME = "22:35";
    private static final String SOURCE_DATA = "http://www.aena.es/csee/Satellite/infovuelos/en/";
    private static final String SOURCE_FLIGHT_STATUS_INFO = "The flight landed";
    private static final String SOURCE_TERMINAL_INFO = "T1";
    private static final String SOURCE_ARRIVAL_TERMINAL_INFO = "T1";
    private static final String SOURCE_ACTUAL_DEPARTURE_DATE_TIME = "01/02/17 01:10";
    private static final String SOURCE_SCHEDULED_DEPARTURE_DATE = "01/02/17";
    private static final String SOURCE_SCHEDULED_DEPARTURE_TIME = "07:30";
    private static final Long SOURCE_CREATED_AT = 1485905009L;

    private static final LocalDateTime DESTINATION_ESTIMATE_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 2, 1, 11, 25);
    private static final LocalDateTime DESTINATION_ESTIMATE_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 10, 30);
    private static final LocalDateTime DESTINATION_ACTUAL_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 1, 31, 23, 50);
    private static final LocalDateTime DESTINATION_ACTUAL_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 1, 10);
    private static final LocalDateTime DESTINATION_SCHEDULED_ARRIVAL_DATE_TIME = LocalDateTime.of(2017, 1, 31, 22, 35);
    private static final LocalDateTime DESTINATION_SCHEDULED_DEPARTURE_DATE_TIME = LocalDateTime.of(2017, 2, 1, 7, 30);
    private static final LocalDateTime DESTINATION_CREATED_AT = LocalDateTime.of(2017, 1, 31, 23, 23, 29);

    private Transformer transformer;
    private SourceDataFlight sourceDataFlight;

    @Before
    public void setUp() {
        transformer = new TransformerImpl();
        sourceDataFlight = new SourceDataFlight();

        sourceDataFlight.setId(SOURCE_ID);
        sourceDataFlight.setActualArrivalDateTime(SOURCE_ACTUAL_ARRIVAL_DATE_TIME);
        sourceDataFlight.setAircraftNameScheduled(SOURCE_AIRCRAFT_NAME_SCHEDULED);
        sourceDataFlight.setArrivalAirportCodeIATA(SOURCE_ARRIVAL_AIRPORT_CODE_IATA);
        sourceDataFlight.setBaggageInfo(SOURCE_BAGGAGE_INFO);
        sourceDataFlight.setCarrierCodeIATA(SOURCE_CARRIER_CODE_IATA);
        sourceDataFlight.setCarrierNumber(SOURCE_CARRIER_NUMBER);
        sourceDataFlight.setRegistrationCounter(SOURCE_REGISTRATION_COUNTER);
        sourceDataFlight.setDepartureAirportCodeIATA(SOURCE_DEPARTURE_AIRPORT_CODE_IATA);
        sourceDataFlight.setEstimatedArrivalDateTime(SOURCE_ESTIMATE_ARRIVAL_DATE_TIME);
        sourceDataFlight.setEstimatedDepartureDateTime(SOURCE_ESTIMATE_DEPARTURE_DATE_TIME);
        sourceDataFlight.setFlightAirlineCodeIATA(SOURCE_FLIGHT_AIRLINE_CODE_IATA);
        sourceDataFlight.setFlightNumber(SOURCE_FLIGHT_NUMBER);
        sourceDataFlight.setFlightLegSequenceId(SOURCE_FLIGHT_LEG_SEQUENCE_ID);
        sourceDataFlight.setGateInfo(SOURCE_GATE_INFO);
        sourceDataFlight.setLoungeInfo(SOURCE_LOUNGE_INFO);
        sourceDataFlight.setScheduledArrivalDate(SOURCE_SCHEDULED_ARRIVAL_DATE);
        sourceDataFlight.setScheduledArrivalTime(SOURCE_SCHEDULED_ARRIVAL_TIME);
        sourceDataFlight.setSourceData(SOURCE_DATA);
        sourceDataFlight.setFlightStatusInfo(SOURCE_FLIGHT_STATUS_INFO);
        sourceDataFlight.setTerminalInfo(SOURCE_TERMINAL_INFO);
        sourceDataFlight.setArrivalTerminalInfo(SOURCE_ARRIVAL_TERMINAL_INFO);
        sourceDataFlight.setCreatedAt(SOURCE_CREATED_AT);
        sourceDataFlight.setActualDepartureDateTime(SOURCE_ACTUAL_DEPARTURE_DATE_TIME);
        sourceDataFlight.setScheduledDepartureDate(SOURCE_SCHEDULED_DEPARTURE_DATE);
        sourceDataFlight.setScheduledDepartureTime(SOURCE_SCHEDULED_DEPARTURE_TIME);
    }

    @Test
    public void transformTest() {
        DestinationDataFlight destinationDataFlight = transformer.transform(sourceDataFlight);

        assertEquals(SOURCE_ID, destinationDataFlight.getId());
        assertEquals(SOURCE_DEPARTURE_AIRPORT_CODE_IATA, destinationDataFlight.getDepartureAirportCodeIATA());
        assertEquals(SOURCE_ARRIVAL_AIRPORT_CODE_IATA, destinationDataFlight.getArrivalAirportCodeIATA());
        assertEquals(SOURCE_FLIGHT_AIRLINE_CODE_IATA, destinationDataFlight.getFlightAirlineCodeIATA());
        assertEquals(SOURCE_FLIGHT_NUMBER, destinationDataFlight.getFlightNumber());
        assertEquals(SOURCE_CARRIER_CODE_IATA, destinationDataFlight.getCarrierCodeIATA());
        assertEquals(SOURCE_CARRIER_NUMBER, destinationDataFlight.getCarrierNumber());
        assertEquals(SOURCE_FLIGHT_STATUS_INFO, destinationDataFlight.getFlightStatusInfo());
        assertEquals(SOURCE_AIRCRAFT_NAME_SCHEDULED, destinationDataFlight.getAircraftNameScheduled());
        assertEquals(SOURCE_BAGGAGE_INFO, destinationDataFlight.getBaggageInfo());
        assertEquals(SOURCE_REGISTRATION_COUNTER, destinationDataFlight.getRegistrationCounter());
        assertEquals(SOURCE_GATE_INFO, destinationDataFlight.getGateInfo());
        assertEquals(SOURCE_LOUNGE_INFO, destinationDataFlight.getLoungeInfo());
        assertEquals(SOURCE_TERMINAL_INFO, destinationDataFlight.getTerminalInfo());
        assertEquals(SOURCE_ARRIVAL_TERMINAL_INFO, destinationDataFlight.getArrivalTerminalInfo());
        assertEquals(SOURCE_DATA, destinationDataFlight.getSourceData());

        assertEquals(DESTINATION_ESTIMATE_ARRIVAL_DATE_TIME, destinationDataFlight.getEstimatedArrivalDateTime());
        assertEquals(DESTINATION_ESTIMATE_DEPARTURE_DATE_TIME, destinationDataFlight.getEstimatedDepartureDateTime());
        assertEquals(DESTINATION_ACTUAL_ARRIVAL_DATE_TIME, destinationDataFlight.getActualArrivalDateTime());
        assertEquals(DESTINATION_ACTUAL_DEPARTURE_DATE_TIME, destinationDataFlight.getActualDepartureDateTime());
        assertEquals(DESTINATION_SCHEDULED_ARRIVAL_DATE_TIME, destinationDataFlight.getScheduledArrivalDateTime());
        assertEquals(DESTINATION_SCHEDULED_DEPARTURE_DATE_TIME, destinationDataFlight.getScheduledDepartureDateTime());

        assertEquals(SOURCE_FLIGHT_LEG_SEQUENCE_ID, destinationDataFlight.getFlightLegSequenceId().toString());

        assertEquals(DESTINATION_CREATED_AT, destinationDataFlight.getCreatedAt().toLocalDateTime());
    }
}